package com.example.zzeulki.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


public class Act1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("Act1","강슬기");
    }

   @Override
    public void onStart()
    {

        Log.i("Act1","onStart");
        super.onStart();
    }

    @Override
    public void onPause()
    {
        Log.i("Act1","onPause");
        super.onPause();
    }

    @Override
    public void onStop()
    {
        Log.i("Act1","onStop");
        super.onStop();
    }

    @Override
    public void onResume()
    {
        Log.i("Act1","onResume");
        super.onResume();
    }

    @Override
    public void onRestart()
    {
        Log.i("Act1","onRestart");
        super.onRestart();
    }

    @Override
    public void onDestroy()
    {
        Log.i("Act1","onDestroy");
        super.onDestroy();
    }
}
